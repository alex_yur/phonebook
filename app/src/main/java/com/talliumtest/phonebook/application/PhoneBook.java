package com.talliumtest.phonebook.application;

import android.app.Application;
import android.content.SharedPreferences;

import com.orm.SugarContext;
import com.talliumtest.phonebook.dagger.DaggerDataBaseComponent;
import com.talliumtest.phonebook.dagger.DataBaseComponent;
import com.talliumtest.phonebook.dagger.DataBaseModule;
import com.talliumtest.phonebook.utils.Constants;
import com.talliumtest.phonebook.utils.DBSeed;

public class PhoneBook extends Application {
    private static DataBaseComponent dataBaseComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);

        createComponent();

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SP, MODE_PRIVATE);

        if(sharedPreferences.getBoolean(Constants.FIRST_LAUNCH, true)){
            new DBSeed(this).seed();
            sharedPreferences.edit().putBoolean(Constants.FIRST_LAUNCH, false).apply();
        }

    }

    private void createComponent(){
        dataBaseComponent =
                DaggerDataBaseComponent.builder().dataBaseModule(new DataBaseModule()).build();
    }

    public static DataBaseComponent getDataBaseComponent(){
        return dataBaseComponent;
    }

}
