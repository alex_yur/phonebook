package com.talliumtest.phonebook.dagger;

import com.squareup.otto.Bus;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.dao.CredentialsDAO;
import com.talliumtest.phonebook.dao.CredentialsDAOImpl;
import com.talliumtest.phonebook.dao.SugarDAOImpl;
import com.talliumtest.phonebook.model.Author;
import com.talliumtest.phonebook.model.Contact;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataBaseModule {

    @Singleton
    @Provides
    public AbstractDAO<Contact> getContactDao(){
        return new SugarDAOImpl<>(Contact.class);
    }

    @Singleton
    @Provides
    public AbstractDAO<Author> getAuthorDao(){
        return new SugarDAOImpl<>(Author.class);
    }

    @Singleton
    @Provides
    public CredentialsDAO getCredentialsDao(){
        return new CredentialsDAOImpl();
    }

    @Singleton
    @Provides
    public Bus getBus(){
        return new Bus();
    }
}
