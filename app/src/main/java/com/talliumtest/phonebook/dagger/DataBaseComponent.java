package com.talliumtest.phonebook.dagger;

import com.squareup.otto.Bus;
import com.talliumtest.phonebook.activities.ManageContactActivity;
import com.talliumtest.phonebook.activities.MainActivity;
import com.talliumtest.phonebook.activities.SplashScreenActivity;
import com.talliumtest.phonebook.adapters.CardViewContactsAdapter;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.dao.CredentialsDAO;
import com.talliumtest.phonebook.dialogs.AboutAuthorDialog;
import com.talliumtest.phonebook.model.Author;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.utils.FabButtonHide;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {DataBaseModule.class})
public interface DataBaseComponent {

    void inject(MainActivity object);
    void inject(ManageContactActivity acd);
    void inject(CardViewContactsAdapter acd);
    void inject(SplashScreenActivity ssa);
    void inject(AboutAuthorDialog aad);

    AbstractDAO<Contact> getContactDao();
    AbstractDAO<Author> getAuthorDao();
    CredentialsDAO getCredentialsDao();
    Bus getBus();
}
