package com.talliumtest.phonebook.utils;

public class Constants {
    public static final String FIRST_LAUNCH = "firstLaunch";

    public static final String SP = "sp";

    public static final String CONTACT_ID = "contactId";
}
