package com.talliumtest.phonebook.utils;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.model.Author;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.model.ContactImage;
import com.talliumtest.phonebook.model.Credentials;

public class DBSeed {
    private Context context;

    public DBSeed(Context context) {
        this.context = context;
    }

    public void seed(){
        seedContacts();
        seedCredentials();
        seedAuthor();
    }

    private void seedCredentials(){
        Credentials.save(new Credentials("admin", "123456"));
    }

    private void seedContacts(){
        Contact contact1 = new Contact("Ivan", "0504443322");
        Contact contact2 = new Contact("Sveta", "0503332242");
        Contact contact3 = new Contact("Max", "0504234222");
        Contact contact4 = new Contact("Oleg", "0504849342");
        Contact contact5 = new Contact("Inokentiy", "0504443322");
        Contact contact6 = new Contact("Petro", "0503332242");
        Contact contact7 = new Contact("Borat", "0504234222");

        Contact.save(contact1);
        Contact.save(contact2);
        Contact.save(contact3);
        Contact.save(contact4);
        Contact.save(contact5);
        Contact.save(contact6);
        Contact.save(contact7);

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_1)), contact1));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_2)), contact2));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_3)), contact3));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_4)), contact4));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_5)), contact5));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_6)), contact6));

        ContactImage.save(new ContactImage(ImageUtil.getBytes(BitmapFactory.decodeResource(context.getResources(),
                R.drawable.dummy_7)), contact7));



    }

    private void seedAuthor(){
        Author.save(
                new Author(
                        "Aleksey Yur",
                        "https://www.linkedin.com/in/aleksey-yur-80224760",
                        "https://www.facebook.com/ayur.vinz",
                        "ayur.vinz@gmail.com",
                        "0504762222"));
    }
}
