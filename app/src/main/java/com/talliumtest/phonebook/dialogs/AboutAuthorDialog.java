package com.talliumtest.phonebook.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.otto.Bus;
import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.R2;
import com.talliumtest.phonebook.application.PhoneBook;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.model.Author;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.ottoevents.ContactAdded;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutAuthorDialog {

    private MaterialDialog materialDialog;
    private Context context;

    @Inject
    AbstractDAO<Author> authorDao;

    @BindView(R2.id.name) TextView name;
    @BindView(R2.id.email) TextView email;
    @BindView(R2.id.phone) TextView phone;
    @BindView(R2.id.linked_in) TextView linkedIn;
    @BindView(R2.id.facebook) TextView facebook;


    public AboutAuthorDialog(Context context) {
        this.context = context;
        PhoneBook.getDataBaseComponent().inject(this);

        initDialog();
    }

    private void initDialog(){
        materialDialog = new MaterialDialog.Builder(context)
                .customView(R.layout.dialog_about, false)
                .negativeText("Ok")
                .build();

        initDialogFields(materialDialog.getCustomView());
    }

    private void initDialogFields(View dialogView){
        ButterKnife.bind(this, dialogView);
        fillData();
    }

    private void fillData(){
        Author author = authorDao.getAll().get(0);

        name.setText(author.getName());
        email.setText(author.getEmail());
        phone.setText(author.getPhone());
        linkedIn.setText(author.getLinkedInLink());
        facebook.setText(author.getFacebookLink());

        linkedIn.setMovementMethod(LinkMovementMethod.getInstance());
        facebook.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void show(){
        materialDialog.show();
    }

}
