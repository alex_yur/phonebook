package com.talliumtest.phonebook.dao;

import com.talliumtest.phonebook.model.Contact;

import java.util.List;

public interface AbstractDAO<T> {
    long save(T contact);
    boolean delete(T contact);
    long edit(T contact);
    T getById(long id);
    List<T> getAll();
}
