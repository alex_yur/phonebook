package com.talliumtest.phonebook.dao;

import com.talliumtest.phonebook.model.Credentials;

public class CredentialsDAOImpl extends SugarDAOImpl<Credentials> implements CredentialsDAO {

    public CredentialsDAOImpl(){
        super(Credentials.class);
    }

    @Override
    public Credentials getCredentialByLogin(String login) {
        return Credentials.findWithQuery(Credentials.class, "SELECT * FROM CREDENTIALS WHERE LOGIN = ?", login).get(0);
    }
}
