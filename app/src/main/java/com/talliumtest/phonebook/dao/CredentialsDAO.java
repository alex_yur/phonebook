package com.talliumtest.phonebook.dao;

import com.talliumtest.phonebook.model.Credentials;

public interface CredentialsDAO extends AbstractDAO<Credentials> {
    Credentials getCredentialByLogin(String login);
}
