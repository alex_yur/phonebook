package com.talliumtest.phonebook.dao;

import com.orm.SugarRecord;

import java.util.List;

public class SugarDAOImpl<T extends SugarRecord> implements AbstractDAO<T> {

    private Class<T> type;

    @SuppressWarnings("unchecked")
    public SugarDAOImpl(Class<T> entity){
        this.type = entity;
    }

    @Override
    public long save(T contact) {
        return contact.save();
    }

    @Override
    public boolean delete(T contact) {
        return contact.delete();
    }

    @Override
    public long edit(T contact) {
        return contact.save();
    }

    @Override
    public T getById(long id) {
        return T.findById(type, id);
    }

    @Override
    public List<T> getAll() {
        return T.listAll(type);
    }
}
