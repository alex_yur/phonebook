package com.talliumtest.phonebook.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.otto.Bus;
import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.activities.ManageContactActivity;
import com.talliumtest.phonebook.application.PhoneBook;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.model.ContactImage;
import com.talliumtest.phonebook.ottoevents.ShouldShowFab;
import com.talliumtest.phonebook.utils.Constants;
import com.talliumtest.phonebook.utils.ImageUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardViewContactsAdapter extends RecyclerView.Adapter<CardViewContactsAdapter.ContactViewHolder> {
    private List<Contact> contactList;
    private Context context;

    @Inject
    AbstractDAO<Contact> mContactDao;

    @Inject Bus bus;

    public CardViewContactsAdapter(List<Contact> contactList, Context context) {
        this.contactList = contactList;
        this.context = context;
        PhoneBook.getDataBaseComponent().inject(this);
        bus.register(this);
    }

    @Override
    public CardViewContactsAdapter.ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_contact_item, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        final Contact currentContact = contactList.get(position);

        holder.mContactName.setText(currentContact.getName());
        holder.mContactNumber.setText(currentContact.getPhoneNumber());
        if(currentContact.getContactImage().getImage().length > 0)
            holder.mContactAvatar.setImageBitmap(ImageUtil.getImage(currentContact.getContactImage().getImage()));

        holder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(context)
                        .content("Do you really want ot remove " + currentContact.getName() + " contact?")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                removeContact(currentContact);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .positiveText("Yes")
                        .negativeText("No")
                        .build()
                        .show();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ManageContactActivity.class);
                intent.putExtra(Constants.CONTACT_ID, currentContact.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.contact_avatar) ImageView mContactAvatar;
        @BindView(R.id.delete) ImageView mDelete;
        @BindView(R.id.contact_name) TextView mContactName;
        @BindView(R.id.contact_number) TextView mContactNumber;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void removeContact(Contact contact){
        int position = contactList.indexOf(contact);
        contactList.remove(contact);
        mContactDao.delete(contact);
        ContactImage.delete(contact.getContactImage());
        bus.post(new ShouldShowFab());
        notifyItemRemoved(position);
    }

    public void addContact(final Contact contact){
        contactList.add(contact);
        notifyItemInserted(contactList.size() - 1);
    }

    public void editContact(final Contact newContact, final Contact oldContact){
        int elementIndex = contactList.indexOf(oldContact);
        contactList.set(elementIndex, newContact);
        notifyItemChanged(elementIndex);
    }
}
