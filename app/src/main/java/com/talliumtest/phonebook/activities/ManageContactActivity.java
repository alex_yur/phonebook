package com.talliumtest.phonebook.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.otto.Bus;
import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.R2;
import com.talliumtest.phonebook.application.PhoneBook;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.model.ContactImage;
import com.talliumtest.phonebook.ottoevents.ContactAdded;
import com.talliumtest.phonebook.ottoevents.ContactEdited;
import com.talliumtest.phonebook.utils.Constants;
import com.talliumtest.phonebook.utils.ImageUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ManageContactActivity extends AppCompatActivity {
    public static final int REQUEST_IMAGE_CAPTURE = 534;
    public static final int REQUEST_CAMERA= 234;

    @BindView(R2.id.toolbar) Toolbar mToolbar;

    @BindView(R2.id.contact_image) ImageView contactAvatar;
    @BindView(R2.id.contact_name) EditText contactName;
    @BindView(R2.id.contact_number) EditText contactNumber;

    @Inject AbstractDAO<Contact> mContactDao;
    @Inject Bus bus;

    private boolean hasNewImage = false;
    private Mode mode = Mode.NEW;

    private Contact contactToShow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        ButterKnife.bind(this);
        PhoneBook.getDataBaseComponent().inject(this);
        bus.register(this);

        try{
            showContact(getIntent().getExtras().getLong(Constants.CONTACT_ID));
            mToolbar.setTitle(getString(R.string.edit_contact));
        } catch (NullPointerException npe){
            mToolbar.setTitle(getString(R.string.add_contact));
        }

        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void showContact(long contactId){
        mode = Mode.SHOW;
        contactToShow = mContactDao.getById(contactId);
        contactName.setText(contactToShow.getName());
        contactName.setFocusable(false);
        contactNumber.setText(contactToShow.getPhoneNumber());
        contactNumber.setFocusable(false);
        if(contactToShow.getContactImage().getImage().length > 0){
            contactAvatar.setImageBitmap(ImageUtil.getImage(contactToShow.getContactImage().getImage()));
        }
    }

    @OnClick(R.id.contact_image) void getPicture(){
        if(mode != Mode.SHOW){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission();
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (mode){
            case SHOW:{
                getMenuInflater().inflate(R.menu.contact_edit, menu);
                break;
            }
            default:{
                getMenuInflater().inflate(R.menu.contact_save, menu);
                break;
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:{
                saveContact();
                return true;
            }
            case R.id.action_edit:{
                enterEditMode();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveContact(){
        Contact contactToSave = getContact();
        if(contactToSave != null){
            if(contactToShow != null){
                mContactDao.edit(contactToSave);
                bus.post(new ContactEdited(contactToSave, contactToShow));
            } else {
                long savedContact = mContactDao.save(contactToSave);
                bus.post(new ContactAdded(savedContact));
            }

            handleImage(contactToSave);

            finish();
        }
    }

    private void handleImage(Contact contactToSave){
        if(hasNewImage){
            if(mode == Mode.EDIT){
                try{
                    ContactImage.delete(contactToShow.getContactImage());
                } catch (Throwable t){

                }
            }

            ContactImage.save(new ContactImage(ImageUtil.getBytes(((BitmapDrawable)contactAvatar.getDrawable()).getBitmap()), contactToSave));
        }
    }

    private void enterEditMode(){
        mode = Mode.EDIT;
        contactName.setFocusableInTouchMode(true);
        contactNumber.setFocusableInTouchMode(true);
        invalidateOptionsMenu();
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new MaterialDialog.Builder(this)
                    .content("This permission is required to capture image")
                    .negativeText("No, thnx.")
                    .positiveText("Okay, ask me again.")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            ActivityCompat.requestPermissions(ManageContactActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .build().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
        }
    }

    private Contact getContact(){
        Contact innerContact;

        if(contactToShow != null){
            innerContact = contactToShow.clone();
        } else {
            innerContact = new Contact();
        }

        String contactNameText = contactName.getText().toString();
        String contactNumberText = contactNumber.getText().toString();

        if(contactNameText.isEmpty()){
            contactName.setError("Name should contain characters");
            return null;
        }

        if(contactNumberText.length() < 10){
            contactNumber.setError("Number should contain at least 10 characters");
            return null;
        }

        innerContact.setName(contactNameText);
        innerContact.setPhoneNumber(contactNumberText);
        return innerContact;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            contactAvatar.setImageBitmap(imageBitmap);
            hasNewImage = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPicture();
            } else {
                //Permission not granted.
            }
        } else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    enum Mode{
        NEW, SHOW, EDIT
    }
}
