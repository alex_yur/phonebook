package com.talliumtest.phonebook.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.R2;
import com.talliumtest.phonebook.application.PhoneBook;
import com.talliumtest.phonebook.dao.CredentialsDAO;
import com.talliumtest.phonebook.model.Credentials;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R2.id.login_view) LinearLayout mLoginView;

    @BindView(R2.id.login) EditText mLogin;
    @BindView(R2.id.password) EditText mPassword;
    @BindView(R2.id.submit) Button mSubmit;

    @Inject
    CredentialsDAO mCredentialsDao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        PhoneBook.getDataBaseComponent().inject(this);

        mLoginView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mLoginView.setVisibility(View.GONE);
                mLoginView.setAlpha(0);
                mLoginView.setTranslationY(mLoginView.getHeight());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showLogin();
                    }
                }, 500);
                mLoginView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void showLogin(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLoginView.setVisibility(View.VISIBLE);
                mLoginView.animate()
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .setDuration(500)
                        .translationY(0)
                        .alpha(1)
                        .start();
            }
        });
    }

    @OnClick(R2.id.submit) void submit(){
        if(validateFields()){
            Credentials credentials = mCredentialsDao.getCredentialByLogin(mLogin.getText().toString());
            if(credentials == null){
                mLogin.setError("No such login!");
                mLogin.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if(!mPassword.getText().toString().equals(credentials.getPassword())){
                    mPassword.setError("Password doesn't match!");
                    mPassword.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        } else {
            mLoginView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        }
    }

    private boolean validateFields(){
        if(mLogin.getText().toString().isEmpty()){
            mLogin.setError("Field should not be empty");
            return false;
        }

        if(mPassword.getText().toString().isEmpty()){
            mPassword.setError("Field should not be empty");
            return false;
        }
        return true;
    }
}
