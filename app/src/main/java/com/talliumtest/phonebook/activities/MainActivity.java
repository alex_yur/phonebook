package com.talliumtest.phonebook.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.LinearInterpolator;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.talliumtest.phonebook.R;
import com.talliumtest.phonebook.R2;
import com.talliumtest.phonebook.adapters.CardViewContactsAdapter;
import com.talliumtest.phonebook.application.PhoneBook;
import com.talliumtest.phonebook.dao.AbstractDAO;
import com.talliumtest.phonebook.dialogs.AboutAuthorDialog;
import com.talliumtest.phonebook.model.Contact;
import com.talliumtest.phonebook.ottoevents.ContactAdded;
import com.talliumtest.phonebook.ottoevents.ContactEdited;
import com.talliumtest.phonebook.ottoevents.ShouldShowFab;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R2.id.contacts_list) RecyclerView mRecyclerView;
    @BindView(R2.id.fab) FloatingActionButton fab;

    @Inject AbstractDAO<Contact> contactsDao;
    @Inject Bus bus;

    private CardViewContactsAdapter cardViewContactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PhoneBook.getDataBaseComponent().inject(this);
        bus.register(this);

        initContactsList();
    }

    private void initContactsList(){
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(llm);
        cardViewContactsAdapter = new CardViewContactsAdapter(contactsDao.getAll(), this);
        mRecyclerView.setAdapter(cardViewContactsAdapter);
    }

    @Subscribe public void showFab(ShouldShowFab ssf){
        fab.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
    }

    @Subscribe public void addToList(ContactAdded contactAdded){
        cardViewContactsAdapter.addContact(contactsDao.getById(contactAdded.getContactId()));
    }

    @Subscribe public void updateElementList(ContactEdited contactEdited){
        cardViewContactsAdapter.editContact(
                contactEdited.getContact(),
                contactEdited.getOldContact()
        );
    }

    @OnClick(R.id.fab) void addContact(){
        Intent intent = new Intent(MainActivity.this, ManageContactActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            new AboutAuthorDialog(this).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }
}
