package com.talliumtest.phonebook.ottoevents;

public class ContactAdded {
    private long contactId;

    public ContactAdded(long contactId) {
        this.contactId = contactId;
    }

    public long getContactId() {
        return contactId;
    }
}
