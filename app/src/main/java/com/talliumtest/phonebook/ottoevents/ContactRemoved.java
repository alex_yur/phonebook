package com.talliumtest.phonebook.ottoevents;

import com.talliumtest.phonebook.model.Contact;

public class ContactRemoved {
    private Contact contact;

    public ContactRemoved(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }
}
