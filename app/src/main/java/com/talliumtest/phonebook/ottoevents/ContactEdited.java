package com.talliumtest.phonebook.ottoevents;

import com.talliumtest.phonebook.model.Contact;

public class ContactEdited {
    private Contact contact;
    private Contact oldContact;

    public ContactEdited(Contact contact, Contact oldContact) {
        this.contact = contact;
        this.oldContact = oldContact;
    }

    public Contact getContact() {
        return contact;
    }

    public Contact getOldContact() {
        return oldContact;
    }
}
