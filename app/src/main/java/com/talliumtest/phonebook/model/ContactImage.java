package com.talliumtest.phonebook.model;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

public class ContactImage extends SugarRecord {
    private byte[] image = new byte[0];
    private Contact contact;

    public ContactImage() {
    }

    public ContactImage(byte[] image, Contact contact) {
        this.image = image;
        this.contact = contact;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
