package com.talliumtest.phonebook.model;

import com.orm.SugarRecord;


public class Author extends SugarRecord {

    private String name;
    private String linkedInLink;
    private String facebookLink;
    private String email;
    private String phone;

    public Author() {
    }

    public Author(String name, String linkedInLink, String facebookLink, String email, String phone) {
        this.name = name;
        this.linkedInLink = linkedInLink;
        this.facebookLink = facebookLink;
        this.email = email;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkedInLink() {
        return linkedInLink;
    }

    public void setLinkedInLink(String linkedInLink) {
        this.linkedInLink = linkedInLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
